const details = document.querySelectorAll('details')
details?.forEach((detail) => {
  detail.addEventListener('toggle', function () {
    if (this.open) {
      details.forEach((otherDetail) => {
        if (otherDetail !== this) {
          otherDetail.removeAttribute('open')
        }
      })
    }
  })
})