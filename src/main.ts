import data from './content.json'
import type { Card } from '@/components/CardComp.ts'
import * as dayjs from 'dayjs'
import * as localizedFormat from 'dayjs/plugin/localizedFormat'

dayjs.extend(localizedFormat)
dayjs.locale('fr')

const grid = document.querySelector('app-grid')!
export const cards = Object.keys(data)
                           .reduce<Card[]>((acc, key) => {
                             return [...acc, ...Object.keys(data[key as keyof typeof data])
                                                      .map<Card>(subKey => {
                                                        const card = data[key as keyof typeof data]
                                                        return {
                                                          ...card[subKey as keyof typeof card],
                                                          slug: subKey,
                                                          createdAt: dayjs(card[subKey as keyof typeof card].createdAt),
                                                          type: key,
                                                        }
                                                      })]
                           }, [])
                           .sort((a, b) => b.createdAt.isBefore(a.createdAt) ? -1 : (b.createdAt.isAfter(a.createdAt) ? 1 : 0))
                           .reduce<Record<string, Card>>((acc, card) => {
                             const cardComp = document.createElement('card-comp')
                             cardComp.setAttribute('type', card.type)
                             cardComp.setAttribute('slug', card.slug)

                             grid.appendChild(cardComp)

                             return { ...acc, [`${card.type}-${card.slug}`]: card }
                           }, {})
