import AppNavStyle from '@/style/AppNav.css?inline'

export class AppNav extends HTMLElement {
  constructor() {
    super()
    this.attachShadow({ mode: 'open' })


    this.shadowRoot!.innerHTML = `
      <style>
      ${AppNavStyle}
      </style>
      <nav>
        <ul>
          <li>
            <a href="/">
              Actualités
            </a>
          </li>
          <li>
            <a href="/?filter=html">
              HTML
            </a>
          </li>
          <li>
            <a href="/?filter=css">
              CSS
            </a>
          </li>
          <li>
            <a href="/?filter=js">
              JS
            </a>
          </li>
        </ul>
      </nav>
    `
  }
}