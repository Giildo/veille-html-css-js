import CardCompStyle from '@/style/CardComp.css?inline'
import type { Dayjs } from 'dayjs'
import { cards } from '@/main.ts'

export interface Card {
  abstract: string;
  createdAt: Dayjs;
  slug: string;
  title: string;
  type: string;
}

export interface CardData {
  title: string;
  abstract: string;
  createdAt: string;
}

export class CardComp extends HTMLElement {
  constructor() {
    super()
    this.attachShadow({ mode: 'open' })

    this.shadowRoot!.innerHTML = `
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@7.2.96/css/materialdesignicons.min.css" rel="stylesheet"/>
    <style>
    ${CardCompStyle}
    </style>
    <article class="${this.getAttribute('type')}_card">
    </article>
    `
  }

  static get observedAttributes() {
    return ['type', 'slug']
  }

  connectedCallback() {
    const icons = {
      html: 'language-html5',
      css: 'language-css3',
      js: 'nodejs',
    }

    const type = this.getAttribute('type')!
    const slug = this.getAttribute('slug')!
    const datumElement = cards[`${type}-${slug}`]

    const article = this.shadowRoot!.querySelector('article')!
    article.classList.add(`${this.getAttribute('type')}_card`)
    article.innerHTML = `
    <a href="/pages/${type}/${slug}.html">
      <hgroup>
        <h2>
          <span>${datumElement?.title}</span>
          <span>
            <i class="mdi mdi-${icons[(this.getAttribute('type') ?? 'html') as keyof typeof icons]}"></i>
          </span>
        </h2>
        <p>${datumElement?.createdAt.format('LL')}</p>
      </hgroup>
      <p>${datumElement?.abstract}</p>
    </a>
    `
  }

}