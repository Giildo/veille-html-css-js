import AppGridStyle from '@/style/AppGrid.css?inline'

export class AppGrid extends HTMLElement {
  constructor() {
    super()

    this.attachShadow({ mode: 'open' })

    this.shadowRoot!.innerHTML = `
      <style>
        ${AppGridStyle}
      </style>
      <main>
        <slot></slot>
      </main>
    `
  }
}