import { CardComp } from '@/components/CardComp.ts'
import { AppGrid } from '@/components/AppGrid.ts'

customElements.define('app-grid', AppGrid)
customElements.define('card-comp', CardComp)