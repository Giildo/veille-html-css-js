import { defineConfig } from 'vite'
import { fileURLToPath } from 'node:url'
import { extname, relative, resolve } from 'node:path'
import { glob } from 'glob'

export default defineConfig({
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '#': fileURLToPath(new URL('./pages', import.meta.url)),
    },
  },
  build: {
    rollupOptions: {
      input: {
        index: fileURLToPath(new URL('./index.html', import.meta.url)),
        ...Object.fromEntries(
            glob.sync(resolve('pages', '**/*.html')).map(file => [
              // This remove `src/` as well as the file extension from each
              // file, so e.g. src/nested/foo.js becomes nested/foo
              relative(
                  'pages',
                  file.slice(0, file.length - extname(file).length),
              ),
              // This expands the relative paths to absolute paths, so e.g.
              // src/nested/foo becomes /project/src/nested/foo.js
              fileURLToPath(new URL(file, import.meta.url)),
            ]),
        ),
      },
    },
  },
})